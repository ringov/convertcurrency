package com.ringov.convertcurrency.core.mvp

import android.util.Log
import java.lang.ref.WeakReference

class MvpDelegate<V : MvpView, P : MvpPresenter<V>>(view: V, private val presenter: P) {

    companion object {
        private const val TAG = "MvpDelegate"
    }

    private val weakView: WeakReference<V> = WeakReference(view)

    fun attach() {
        val view = weakView.get()
        if (view != null) {
            presenter.attachView(view)
        } else {
            Log.w(TAG, "MvpDelegate.attach has been called with null view reference")
        }
    }

    fun detach() {
        presenter.detach()
    }
}
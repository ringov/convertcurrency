package com.ringov.convertcurrency.core.mvp

import androidx.annotation.CallSuper

abstract class MvpPresenter<V : MvpView> {

    private var view: V? = null

    protected fun getView() = view

    @CallSuper
    open fun attachView(view: V) {
        this.view = view
    }

    @CallSuper
    open fun detach() {
        view = null
    }
}
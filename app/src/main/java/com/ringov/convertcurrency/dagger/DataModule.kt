package com.ringov.convertcurrency.dagger

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.ringov.convertcurrency.BuildConfig
import com.ringov.convertcurrency.data.*
import com.ringov.convertcurrency.domain.ApiRequestRetryHandler
import com.ringov.convertcurrency.utils.RxSchedulers
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class DataModule {

    companion object {
        private const val SHARED_PREFERENCES_NAME = "${BuildConfig.APPLICATION_ID}.shared_prefs"

        private const val RATES_NAMED = "rates"
        private const val CURRENCIES_NAMED = "currencies"
    }

    @Provides
    fun provideSharePreferences(context: Context): SharedPreferences =
        context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)

    @Provides
    fun providePersistentStorage(sharedPreferences: SharedPreferences, gson: Gson) =
        PersistentStorage(sharedPreferences, gson)

    @Singleton
    @Provides
    fun provideCacheStorage() = CacheStorage()

    @Provides
    fun provideClient() = OkHttpClient()

    @Provides
    fun provideRxJavaAdapter(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    @Provides
    fun provideGsonConverter(gson: Gson): GsonConverterFactory = GsonConverterFactory.create(gson)

    @Named(RATES_NAMED)
    @Provides
    fun provideRatesRetrofit(
        client: OkHttpClient,
        rxJavaAdapter: RxJava2CallAdapterFactory,
        gsonConverter: GsonConverterFactory
    ): Retrofit = Retrofit.Builder().baseUrl(RatesApi.BASE_URL)
        .addCallAdapterFactory(rxJavaAdapter)
        .addConverterFactory(gsonConverter)
        .client(client).build()

    @Named(CURRENCIES_NAMED)
    @Provides
    fun provideCurrenciesRetrofit(
        client: OkHttpClient,
        rxJavaAdapter: RxJava2CallAdapterFactory,
        gsonConverter: GsonConverterFactory
    ): Retrofit = Retrofit.Builder().baseUrl(CurrenciesApi.BASE_URL)
        .addCallAdapterFactory(rxJavaAdapter)
        .addConverterFactory(gsonConverter)
        .client(client).build()

    @Provides
    fun provideRatesApi(@Named(RATES_NAMED) retrofit: Retrofit): RatesApi = retrofit.create(RatesApi::class.java)

    @Provides
    fun provideCurrenciesApi(@Named(CURRENCIES_NAMED) retrofit: Retrofit): CurrenciesApi =
        retrofit.create(CurrenciesApi::class.java)

    @Provides
    fun provideCurrenciesRepo(
        currenciesApi: CurrenciesApi, persistentStorage: PersistentStorage,
        retryHandler: ApiRequestRetryHandler, rxSchedulers: RxSchedulers
    ) = CurrenciesRepo(currenciesApi, persistentStorage, retryHandler, rxSchedulers)
}
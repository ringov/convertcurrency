package com.ringov.convertcurrency.dagger

import com.ringov.convertcurrency.data.CacheStorage
import com.ringov.convertcurrency.data.CurrenciesRepo
import com.ringov.convertcurrency.data.PersistentStorage
import com.ringov.convertcurrency.data.RatesApi
import com.ringov.convertcurrency.domain.ApiRequestRetryHandler
import com.ringov.convertcurrency.domain.ConvertManager
import com.ringov.convertcurrency.domain.NetworkConnectionManager
import com.ringov.convertcurrency.domain.usecases.*
import com.ringov.convertcurrency.utils.RxSchedulers
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DomainModule {

    @Singleton
    @Provides
    fun provideCurrentCurrencyUsecase(storage: PersistentStorage) =
        CurrentCurrencyUsecase(storage)

    @Singleton
    @Provides
    fun provideCurrencyPriorityUsecase(storage: PersistentStorage, rxSchedulers: RxSchedulers) =
        CurrencyPriorityUsecase(storage, rxSchedulers)

    @Singleton
    @Provides
    fun provideNetworkConnectionManager() = NetworkConnectionManager()

    @Provides
    fun provideGetExchangeRatesUsecase(
        ratesApi: RatesApi,
        currentCurrencyUsecase: CurrentCurrencyUsecase,
        networkConnectionManager: NetworkConnectionManager,
        retryHandler: ApiRequestRetryHandler,
        rxSchedulers: RxSchedulers
    ) = GetExchangeRatesUsecase(ratesApi, currentCurrencyUsecase, networkConnectionManager, retryHandler, rxSchedulers)

    @Provides
    fun provideCurrencyValuesUsecase(
        currentCurrencyUsecase: CurrentCurrencyUsecase,
        getExchangeRatesUsecase: GetExchangeRatesUsecase,
        convertManager: ConvertManager,
        cacheStorage: CacheStorage,
        schedulers: RxSchedulers
    ) = CurrencyValuesUsecase(currentCurrencyUsecase, getExchangeRatesUsecase, convertManager, cacheStorage, schedulers)

    @Provides
    fun provideSortedCurrencyValuesUsecase(
        currencyValuesUsecase: CurrencyValuesUsecase,
        currencyPriorityUsecase: CurrencyPriorityUsecase,
        currenciesRepo: CurrenciesRepo,
        rxSchedulers: RxSchedulers
    ) = PrioritySortedCurrencyValuesUsecase(
        currencyValuesUsecase,
        currencyPriorityUsecase,
        currenciesRepo,
        rxSchedulers
    )

    @Provides
    fun provideConvertManager() = ConvertManager()

    @Provides
    fun provideApiRequestRetryHandler() = ApiRequestRetryHandler()
}
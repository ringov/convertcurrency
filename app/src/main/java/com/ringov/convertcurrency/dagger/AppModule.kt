package com.ringov.convertcurrency.dagger

import android.app.Application
import android.content.Context
import com.ringov.convertcurrency.App
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val app: App) {

    @Provides
    fun provideApplication(): Application = app

    @Provides
    fun provideContext(application: Application): Context = application
}

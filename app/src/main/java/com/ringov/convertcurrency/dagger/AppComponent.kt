package com.ringov.convertcurrency.dagger

import com.ringov.convertcurrency.ui.MainActivity
import com.ringov.convertcurrency.ui.rates.RatesFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, PresentationModule::class, DomainModule::class, DataModule::class, UtilsModule::class])
interface AppComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(ratesFragment: RatesFragment)
}
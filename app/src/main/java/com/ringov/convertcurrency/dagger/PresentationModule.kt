package com.ringov.convertcurrency.dagger

import com.ringov.convertcurrency.domain.NetworkConnectionManager
import com.ringov.convertcurrency.domain.usecases.CurrencyPriorityUsecase
import com.ringov.convertcurrency.domain.usecases.CurrentCurrencyUsecase
import com.ringov.convertcurrency.domain.usecases.PrioritySortedCurrencyValuesUsecase
import com.ringov.convertcurrency.ui.rates.RatesPresenter
import com.ringov.convertcurrency.utils.RxSchedulers
import dagger.Module
import dagger.Provides

@Module
class PresentationModule {

    @Provides
    fun provideRatesPresenter(
        currentCurrencyUsecase: CurrentCurrencyUsecase,
        currencyPriorityUsecase: CurrencyPriorityUsecase,
        prioritySortedCurrencyValuesUsecase: PrioritySortedCurrencyValuesUsecase,
        networkConnectionManager: NetworkConnectionManager,
        schedulers: RxSchedulers
    ) = RatesPresenter(
        currentCurrencyUsecase,
        currencyPriorityUsecase,
        prioritySortedCurrencyValuesUsecase,
        networkConnectionManager,
        schedulers
    )

}
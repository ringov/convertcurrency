package com.ringov.convertcurrency.ui.base

import androidx.annotation.CallSuper
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class RxPresenter<V : BaseView> : BasePresenter<V>() {

    private val disposables = CompositeDisposable()

    protected fun Disposable.observe() {
        disposables.add(this)
    }

    @CallSuper
    override fun detach() {
        super.detach()
        disposables.clear()
    }
}
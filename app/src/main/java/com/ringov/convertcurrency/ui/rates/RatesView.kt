package com.ringov.convertcurrency.ui.rates

import com.ringov.convertcurrency.domain.CurrencyValue
import com.ringov.convertcurrency.ui.base.BaseView

interface RatesView : BaseView {
    fun showRates(rates: List<CurrencyValue>)
    fun updateConnectivity(connected: Boolean)
}
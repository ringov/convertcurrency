package com.ringov.convertcurrency.ui.base

import com.ringov.convertcurrency.core.mvp.MvpPresenter

abstract class BasePresenter<V : BaseView> : MvpPresenter<V>()
package com.ringov.convertcurrency.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.ringov.convertcurrency.App
import com.ringov.convertcurrency.R
import com.ringov.convertcurrency.domain.NetworkConnectionManager
import com.ringov.convertcurrency.ui.about.AboutActivity
import com.ringov.convertcurrency.ui.base.Injectable
import com.ringov.convertcurrency.ui.rates.RatesFragment
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), Injectable {

    @Inject
    lateinit var networkConnectionManager: NetworkConnectionManager

    override fun inject() {
        App.component.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupToolbar()
        showRatesFragment()
    }

    override fun onStart() {
        super.onStart()
        networkConnectionManager.register(this)
    }

    override fun onStop() {
        networkConnectionManager.unregister(this)
        super.onStop()
    }

    private fun showRatesFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, RatesFragment(), RatesFragment::class.java.name).commit()
    }

    private fun setupToolbar() {
        setSupportActionBar(custom_toolbar)
        actionBar?.setTitle(R.string.app_name)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId ?: 0
        if (id == R.id.about_menu) {
            openAbout()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openAbout() {
        startActivity(Intent(this, AboutActivity::class.java))
    }
}

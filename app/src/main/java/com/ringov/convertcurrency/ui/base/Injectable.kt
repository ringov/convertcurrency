package com.ringov.convertcurrency.ui.base

interface Injectable {
    fun inject()
}
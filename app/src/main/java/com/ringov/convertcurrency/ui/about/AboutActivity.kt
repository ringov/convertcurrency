package com.ringov.convertcurrency.ui.about

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.ringov.convertcurrency.BuildConfig
import com.ringov.convertcurrency.R
import com.ringov.convertcurrency.utils.Utils
import com.ringov.convertcurrency.utils.setLinkableText
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        initBar()
        initAppTitle()
        initBuildVersion()
        initPrivacyPolicy()
    }

    private fun initBar() {
        val bar = supportActionBar
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true)
            bar.setTitle(R.string.about_title)
        }
    }

    private fun initAppTitle() {
        app_title_view.setLinkableText(getString(R.string.app_name), Utils.getAppPlayLink(this))
    }

    private fun initBuildVersion() {
        version_build_view.text = getString(
            R.string.build_version,
            BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE.toString()
        )
    }

    private fun initPrivacyPolicy() {
        privacy_view.setLinkableText(R.string.privacy_text, R.string.privacy_link)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
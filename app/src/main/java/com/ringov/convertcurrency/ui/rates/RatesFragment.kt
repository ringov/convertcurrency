package com.ringov.convertcurrency.ui.rates

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.ringov.convertcurrency.App
import com.ringov.convertcurrency.R
import com.ringov.convertcurrency.core.mvp.MvpDelegate
import com.ringov.convertcurrency.domain.CurrencyValue
import com.ringov.convertcurrency.ui.base.BaseFragment
import com.ringov.convertcurrency.utils.gone
import com.ringov.convertcurrency.utils.hideKeyboard
import com.ringov.convertcurrency.utils.visible
import kotlinx.android.synthetic.main.fragment_rates.*
import javax.inject.Inject


class RatesFragment : BaseFragment(), RatesView {

    @Inject
    lateinit var presenter: RatesPresenter
    private lateinit var adapter: RatesAdapter

    override fun getLayout() = R.layout.fragment_rates

    override fun inject() {
        App.component.inject(this)
    }

    override fun initViews() {
        super.initViews()
        rv_rates.layoutManager = LinearLayoutManager(context)
        adapter = RatesAdapter(presenter::onCurrencyClicked, presenter::onCurrencySelected, presenter::onValueChanged)
        rv_rates.adapter = adapter
        (rv_rates.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        rv_rates.addOnScrollListener(OnListScrolled(this::deselect))
    }

    override fun onStop() {
        super.onStop()
        deselect()
    }

    override fun provideDelegate(): MvpDelegate<*, *> {
        return MvpDelegate(this, presenter)
    }

    override fun showRates(rates: List<CurrencyValue>) {
        progress_bar.gone()
        rv_rates.visible()
        adapter.update(rates)
    }

    override fun updateConnectivity(connected: Boolean) {
        if (connected) {
            tv_no_connectivity.gone()
        } else {
            tv_no_connectivity.visible()
        }
    }

    private fun deselect() {
        list_container.requestFocus()
        list_container.hideKeyboard()
        presenter.onDeselected()
    }

    private class OnListScrolled(private val scrolled: () -> Unit) : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            scrolled()
        }
    }
}
package com.ringov.convertcurrency.ui.base

import androidx.annotation.LayoutRes

interface LayoutHolder {
    @LayoutRes
    fun getLayout(): Int
}
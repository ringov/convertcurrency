package com.ringov.convertcurrency.ui.rates

import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ringov.convertcurrency.R
import com.ringov.convertcurrency.domain.CurrencyValue
import com.ringov.convertcurrency.utils.hideKeyboard
import com.ringov.convertcurrency.utils.showKeyboard
import java.math.BigDecimal
import java.math.RoundingMode

class RatesAdapter(
    private val clickCallback: (String, BigDecimal) -> Unit,
    private val selectCallback: (String, BigDecimal) -> Unit,
    private val valueChangedCallback: (String, BigDecimal) -> Unit
) :
    ListAdapter<CurrencyValue, RatesAdapter.ViewHolder>(RateValueDiffCallback()) {

    fun update(rates: List<CurrencyValue>) {
        submitList(rates)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.rate_item, parent, false)
        return ViewHolder(v, clickCallback, selectCallback, valueChangedCallback)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    class ViewHolder(
        itemView: View, private val clickCallback: (String, BigDecimal) -> Unit,
        private val selectCallback: (String, BigDecimal) -> Unit,
        valueChangedCallback: (String, BigDecimal) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        companion object {
            private const val PROGRAMMATICALLY_UPDATED_TAG = "prog_update"
            private const val PRECISION = 4
        }

        private val title: TextView = itemView.findViewById(R.id.title)
        private val subtitle: TextView = itemView.findViewById(R.id.subtitle)
        private val input: EditText = itemView.findViewById(R.id.input)

        private val textChangedListener = TextChangedListener(valueChangedCallback)
        private val editorListener = EditorListener()

        fun bind(item: CurrencyValue) {
            title.text = item.currency
            subtitle.text = item.description
            if (!input.hasFocus()) {
                val string = if (item.value.compareTo(BigDecimal.ZERO) != 0) {
                    val tmp = item.value.setScale(PRECISION, RoundingMode.HALF_UP)
                    if (tmp.compareTo(BigDecimal.ZERO) != 0) {
                        tmp.stripTrailingZeros().toPlainString()
                    } else {
                        item.value.stripTrailingZeros().toPlainString()
                    }
                } else {
                    ""
                }
                input.updateTextProgrammatically(string)
                input.listenForSelected { this.onSelected(item, it) }
            } else {
                input.removeSelectedListener()
            }
            itemView.setOnClickListener {
                clickCallback(item.currency, item.value)
                input.focusEnding()
            }
        }

        private fun onSelected(item: CurrencyValue, selected: Boolean) {
            if (selected) {
                input.removeSelectedListener()
                textChangedListener.withCurrency(item.currency).onView(input)
                input.setOnEditorActionListener(editorListener)
                input.showKeyboard()
                selectCallback(item.currency, item.value)
            } else {
                textChangedListener.reset()
            }
        }

        private fun EditText.focusEnding() {
            this.requestFocus()
            this.setSelection(this.text.length)
        }

        private fun EditText.removeSelectedListener() {
            this.onFocusChangeListener = null
        }

        private fun EditText.listenForSelected(callback: (Boolean) -> Unit) {
            this.setOnFocusChangeListener { _, hasFocus -> callback(hasFocus) }
        }

        private fun EditText.updateTextProgrammatically(text: String) {
            this.tag = PROGRAMMATICALLY_UPDATED_TAG
            this.setText(text)
            this.tag = null
        }

        private class EditorListener : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (event != null && (event.keyCode == KeyEvent.KEYCODE_ENTER)) {
                    v?.hideKeyboard()
                }
                return false
            }
        }

        private class TextChangedListener(private val valueChangedCallback: (String, BigDecimal) -> Unit) :
            TextWatcher {

            companion object {
                private const val DOT = "."
            }

            private var view: EditText? = null
            private var currency: String = ""

            fun onView(view: EditText): TextChangedListener {
                view.addTextChangedListener(this)
                this.view = view
                return this
            }

            fun withCurrency(currency: String): TextChangedListener {
                this.currency = currency
                return this
            }

            fun reset() {
                view = null
                currency = ""
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

            override fun afterTextChanged(s: Editable?) {
                val currentView = view ?: throw IllegalStateException("You must specify the view via onView()!")
                if (currentView.isUpdatedProgrammatically()) {
                    return
                }
                s?.toString()?.let {
                    if (it == DOT) {
                        s.clear()
                        return
                    }
                    if (currency.isBlank()) {
                        throw IllegalStateException("You must specify the currency via method withCurrency()!")
                    }
                    val bigDecimal = if (it.isNotBlank()) BigDecimal(it) else BigDecimal.ZERO
                    valueChangedCallback(currency, bigDecimal)
                }
            }

            private fun EditText.isUpdatedProgrammatically(): Boolean = this.tag == PROGRAMMATICALLY_UPDATED_TAG
        }
    }
}
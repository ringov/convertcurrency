package com.ringov.convertcurrency.ui.rates

import com.ringov.convertcurrency.domain.CurrencyValue
import com.ringov.convertcurrency.domain.NetworkConnectionManager
import com.ringov.convertcurrency.domain.usecases.CurrencyPriorityUsecase
import com.ringov.convertcurrency.domain.usecases.CurrentCurrencyUsecase
import com.ringov.convertcurrency.domain.usecases.PrioritySortedCurrencyValuesUsecase
import com.ringov.convertcurrency.ui.base.RxPresenter
import com.ringov.convertcurrency.utils.Logger
import com.ringov.convertcurrency.utils.RxSchedulers
import java.math.BigDecimal

class RatesPresenter(
    private val currentCurrencyUsecase: CurrentCurrencyUsecase,
    private val currencyPriorityUsecase: CurrencyPriorityUsecase,
    private val prioritySortedCurrencyValuesUsecase: PrioritySortedCurrencyValuesUsecase,
    private val networkConnectionManager: NetworkConnectionManager,
    private val schedulers: RxSchedulers
) : RxPresenter<RatesView>() {

    private var priorCurrency: CurrencyValue? = null

    override fun attachView(view: RatesView) {
        super.attachView(view)
        currencyPriorityUsecase.init()
            .subscribe({}) { Logger.e(it) }
            .observe()

        prioritySortedCurrencyValuesUsecase.observe()
            .observeOn(schedulers.mainThread)
            .subscribe(this::updateRates) { Logger.e(it) }
            .observe()

        networkConnectionManager.observe(schedulers.mainThread)
            .subscribe(this::updateConnectivity) { Logger.e(it) }
            .observe()
    }

    fun onCurrencyClicked(currency: String, value: BigDecimal) {
        Logger.d("Clicked: $currency, $value")
        currencyPriorityUsecase.setPriorCurrency(currency)
    }

    fun onCurrencySelected(currency: String, value: BigDecimal) {
        Logger.d("Selected: $currency, $value")
        currentCurrencyUsecase.update(currency, value)
    }

    fun onValueChanged(currency: String, value: BigDecimal) {
        Logger.d("Changed: $currency, $value")
        currentCurrencyUsecase.update(currency, value)
    }

    fun onDeselected() {
        val currency = priorCurrency
        if (currency != null) {
            currentCurrencyUsecase.update(currency.currency, currency.value)
        }
    }

    private fun updateRates(rates: List<CurrencyValue>) {
        priorCurrency = if (rates.isNotEmpty()) rates[0] else null
        if (rates.isNotEmpty()) {
            getView()?.showRates(rates)
        }
    }

    private fun updateConnectivity(connected: Boolean) {
        getView()?.updateConnectivity(connected)
    }
}
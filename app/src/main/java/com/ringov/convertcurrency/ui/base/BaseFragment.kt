package com.ringov.convertcurrency.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.ringov.convertcurrency.core.mvp.MvpDelegate

abstract class BaseFragment : Fragment(), BaseView, LayoutHolder, Injectable {

    private lateinit var delegate: MvpDelegate<*, *>

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(getLayout(), container, false)


    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        delegate = provideDelegate()
        delegate.attach()
    }

    @CallSuper
    override fun onDestroyView() {
        super.onDestroyView()
        delegate.detach()
    }

    /**
     * override for di
     */
    override fun inject() {

    }

    /**
     * override for view initialization
     */
    protected open fun initViews() {

    }

    protected abstract fun provideDelegate(): MvpDelegate<*, *>
}
package com.ringov.convertcurrency.ui.rates

import androidx.recyclerview.widget.DiffUtil
import com.ringov.convertcurrency.domain.CurrencyValue


class RateValueDiffCallback :
    DiffUtil.ItemCallback<CurrencyValue>() {
    override fun areItemsTheSame(oldItem: CurrencyValue, newItem: CurrencyValue): Boolean = oldItem.currency == newItem.currency

    override fun areContentsTheSame(oldItem: CurrencyValue, newItem: CurrencyValue): Boolean = oldItem == newItem
}
package com.ringov.convertcurrency.utils

import android.content.Context
import android.text.method.LinkMovementMethod
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.text.HtmlCompat
import com.ringov.convertcurrency.R


fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.hideKeyboard() {
    if (this.hasFocus()) {
        val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(this.windowToken, 0)
        this.clearFocus()
    }
}

fun View.showKeyboard() {
    val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun TextView.setLinkableText(text: String, link: String) {
    val linkable = context.getString(R.string.linkable, link, text)
    this.text = HtmlCompat.fromHtml(linkable, HtmlCompat.FROM_HTML_MODE_COMPACT)
    this.movementMethod = LinkMovementMethod.getInstance()
}

fun TextView.setLinkableText(@StringRes textRes: Int, @StringRes linkRes: Int) {
    this.setLinkableText(context.getString(textRes), context.getString(linkRes))
}
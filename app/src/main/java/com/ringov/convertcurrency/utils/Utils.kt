package com.ringov.convertcurrency.utils

import android.content.Context
import com.ringov.convertcurrency.BuildConfig
import com.ringov.convertcurrency.R

object Utils {
    fun getAppPlayLink(context: Context): String {
        return context.getString(R.string.play_market_link, BuildConfig.APPLICATION_ID)
    }
}
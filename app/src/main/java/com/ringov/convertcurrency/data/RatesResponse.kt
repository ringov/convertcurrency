package com.ringov.convertcurrency.data

import com.google.gson.annotations.SerializedName

data class RatesResponse(@SerializedName("rates") val rates: Map<String, Double>)
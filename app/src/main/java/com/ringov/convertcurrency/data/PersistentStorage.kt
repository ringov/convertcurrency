package com.ringov.convertcurrency.data

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ringov.convertcurrency.domain.CurrencyValue
import com.ringov.convertcurrency.utils.Logger
import java.math.BigDecimal
import java.util.*
import kotlin.collections.HashMap


class PersistentStorage(private val sharedPreferences: SharedPreferences, private val gson: Gson) {

    companion object {
        private const val SEPARATOR = "|"
        private const val CURRENCY_SORTING_KEY = "currency_sorting"
        private const val CURRENT_CURRENCY_KEY = "current_currency"
        private const val CURRENT_VALUE_KEY = "current_value"
        private const val CURRENCIES_KEY = "currencies"

        private const val DEFAULT_CURRENCY = "EUR"
        private val DEFAULT_VALUE = BigDecimal.ONE
    }

    fun loadCurrencySorting(): LinkedList<String> {
        val raw = sharedPreferences.getString(CURRENCY_SORTING_KEY, DEFAULT_CURRENCY) ?: DEFAULT_CURRENCY
        return LinkedList(raw.split(SEPARATOR))
    }

    fun saveCurrencySorting(sortedList: List<String>) {
        val sb = StringBuilder()
        sortedList.forEach { sb.append(it).append(SEPARATOR) }
        sharedPreferences.edit().putString(CURRENCY_SORTING_KEY, sb.removeSuffix(SEPARATOR).toString()).apply()
    }

    fun loadCurrentCurrency(): CurrencyValue {
        val currency = sharedPreferences.getString(CURRENT_CURRENCY_KEY, DEFAULT_CURRENCY) ?: DEFAULT_CURRENCY
        val valueRaw = sharedPreferences.getString(CURRENT_VALUE_KEY, "") ?: ""
        val value = if (valueRaw.isNotBlank()) BigDecimal(valueRaw) else DEFAULT_VALUE
        return CurrencyValue(currency, value)
    }

    fun saveCurrentCurrency(currentCurrency: CurrencyValue) {
        sharedPreferences.edit().putString(CURRENT_CURRENCY_KEY, currentCurrency.currency).apply()
        sharedPreferences.edit().putString(CURRENT_VALUE_KEY, currentCurrency.value.toPlainString()).apply()
    }

    fun loadCurrencies(): Map<String, String> {
        val raw = sharedPreferences.getString(CURRENCIES_KEY, "") ?: ""
        if (raw.isBlank()) {
            return HashMap()
        }
        val mapType = object : TypeToken<Map<String, String>>() {}.type
        val map = gson.fromJson<Map<String, String>>(raw, mapType)
        Logger.d("Loading currencies: $map")
        return map
    }

    fun saveCurrencies(currencies: Map<String, String>) {
        val raw = gson.toJson(currencies)
        sharedPreferences.edit().putString(CURRENCIES_KEY, raw).apply()
        Logger.d("Saving currencies: $currencies")
    }
}
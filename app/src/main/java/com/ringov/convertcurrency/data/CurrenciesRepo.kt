package com.ringov.convertcurrency.data

import com.ringov.convertcurrency.domain.ApiRequestRetryHandler
import com.ringov.convertcurrency.utils.Logger
import com.ringov.convertcurrency.utils.RxSchedulers
import io.reactivex.Single

class CurrenciesRepo(
    private val currenciesApi: CurrenciesApi, private val persistentStorage: PersistentStorage,
    private val retryHandler: ApiRequestRetryHandler, private val rxSchedulers: RxSchedulers
) {
    fun getCurrencies(): Single<Map<String, String>> =
        Single.fromCallable { persistentStorage.loadCurrencies() }
            .flatMap { local ->
                if (local.isNotEmpty()) {
                    Single.just(local)
                } else {
                    currenciesApi.getCurrencies()
                        .doOnError { Logger.e(it) }
                        .retryWhen(retryHandler)
                        .map { it.body() ?: HashMap() }
                        .doOnSuccess { persistentStorage.saveCurrencies(it) }
                        .doOnSuccess { Logger.d("Currencies: $it") }
                }
            }
            .subscribeOn(rxSchedulers.io)
}
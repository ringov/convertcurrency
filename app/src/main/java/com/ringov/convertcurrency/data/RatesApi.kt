package com.ringov.convertcurrency.data

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {

    companion object {
        const val BASE_URL = "https://api.ratesapi.io/api/"
    }

    @GET("latest")
    fun getRates(@Query("base") baseCurrency: String): Single<Response<RatesResponse>>
}
package com.ringov.convertcurrency.data

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface CurrenciesApi {
    companion object {
        const val BASE_URL = "https://openexchangerates.org"
    }

    @GET("api/currencies.json")
    fun getCurrencies(): Single<Response<Map<String, String>>>
}
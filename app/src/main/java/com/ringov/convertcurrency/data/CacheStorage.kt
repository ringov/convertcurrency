package com.ringov.convertcurrency.data

import java.math.BigDecimal

class CacheStorage {

    private var currencyValues: Map<String, BigDecimal> = HashMap()

    fun putCurrencyValues(values: Map<String, BigDecimal>) {
        currencyValues = values
    }

    fun getCurrencyValues(): Map<String, BigDecimal> = currencyValues

}
package com.ringov.convertcurrency.domain.usecases

import com.ringov.convertcurrency.data.RatesApi
import com.ringov.convertcurrency.data.RatesResponse
import com.ringov.convertcurrency.domain.ApiRequestRetryHandler
import com.ringov.convertcurrency.domain.CurrencyValue
import com.ringov.convertcurrency.domain.NetworkConnectionManager
import com.ringov.convertcurrency.utils.Logger
import com.ringov.convertcurrency.utils.RxSchedulers
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import retrofit2.Response
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class GetExchangeRatesUsecase(
    private val ratesApi: RatesApi, private val currentCurrencyUsecase: CurrentCurrencyUsecase,
    private val networkConnectionManager: NetworkConnectionManager,
    private val retryHandler: ApiRequestRetryHandler,
    rxSchedulers: RxSchedulers
) {

    companion object {
        private const val INTERVAL = 60L
    }

    private val ioScheduler = rxSchedulers.io

    fun observe(): Flowable<Map<String, BigDecimal>> =
        networkConnectionManager.waitForNetwork(ioScheduler)
            .doOnSuccess { Logger.d("Network connected") }
            .flatMapPublisher { Flowable.interval(0, INTERVAL, TimeUnit.SECONDS, ioScheduler) }
            .withLatestFrom(currentCurrencyUsecase.observe(ioScheduler),
                BiFunction<Long, CurrencyValue, String> { _, currencyValue -> currencyValue.currency })
            .flatMapSingle { ratesApi.getRates(it) }
            .doOnError { Logger.e(it) }
            .retryWhen(retryHandler.scheduler(ioScheduler))
            .map(this::convert)
            .withLatestFrom(currentCurrencyUsecase.observe(ioScheduler).map { it.currency },
                BiFunction<MutableMap<String, BigDecimal>, String, Map<String, BigDecimal>> { rates, currency ->
                    rates.apply { this[currency] = BigDecimal.ONE }
                })
            .doOnNext { Logger.d("Exchange rate update: $it") }

    private fun convert(response: Response<RatesResponse>): MutableMap<String, BigDecimal> =
        (response.body()?.rates?.mapValues { BigDecimal.valueOf(it.value) } ?: HashMap()).toMutableMap()
}
package com.ringov.convertcurrency.domain

import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.functions.Function
import org.reactivestreams.Publisher
import java.util.concurrent.TimeUnit

class ApiRequestRetryHandler : Function<Flowable<Throwable>, Publisher<Long>> {

    companion object {
        private const val RETRY_INTERVAL = 1L
    }

    private var scheduler: Scheduler? = null

    fun scheduler(scheduler: Scheduler): ApiRequestRetryHandler {
        this.scheduler = scheduler
        return this
    }

    override fun apply(attempts: Flowable<Throwable>): Publisher<Long> = attempts.switchMap(this::retry)

    private fun retry(throwable: Throwable): Flowable<Long> {
        return if (scheduler != null) Flowable.timer(RETRY_INTERVAL, TimeUnit.SECONDS, scheduler)
        else Flowable.timer(RETRY_INTERVAL, TimeUnit.SECONDS)
    }
}
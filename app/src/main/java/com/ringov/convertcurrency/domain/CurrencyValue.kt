package com.ringov.convertcurrency.domain

import java.math.BigDecimal

data class CurrencyValue(val currency: String, val value: BigDecimal, val description: String = "")
package com.ringov.convertcurrency.domain.usecases

import com.ringov.convertcurrency.data.PersistentStorage
import com.ringov.convertcurrency.utils.RxSchedulers
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.Single
import java.util.*

class CurrencyPriorityUsecase(
    private val storage: PersistentStorage,
    private val rxSchedulers: RxSchedulers
) :
    BaseStateUsecase<LinkedList<String>>() {

    fun init(): Completable = Single.fromCallable { storage.loadCurrencySorting() }
        .doOnSuccess { update(it) }
        .ignoreElement()
        .subscribeOn(rxSchedulers.io)

    fun setPriorCurrency(currency: String) {
        val list = getValue() ?: LinkedList()
        list.remove(currency)
        list.add(currency)
        update(list)
    }

    override fun modify(upstream: Flowable<LinkedList<String>>, scheduler: Scheduler): Flowable<LinkedList<String>> {
        return super.modify(upstream, scheduler)
            .doOnNext { storage.saveCurrencySorting(it) }
    }
}
package com.ringov.convertcurrency.domain.usecases

import com.ringov.convertcurrency.data.CacheStorage
import com.ringov.convertcurrency.domain.ConvertManager
import com.ringov.convertcurrency.domain.CurrencyValue
import com.ringov.convertcurrency.utils.RxSchedulers
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import java.math.BigDecimal

class CurrencyValuesUsecase(
    private val currentCurrencyUsecase: CurrentCurrencyUsecase,
    private val getExchangeRatesUsecase: GetExchangeRatesUsecase,
    private val convertManager: ConvertManager,
    private val storage: CacheStorage,
    private val schedulers: RxSchedulers
) {
    fun observe(): Flowable<Map<String, BigDecimal>> {
        return Flowable.combineLatest(getExchangeRatesUsecase.observe(), currentCurrencyUsecase.observe(schedulers.io),
            BiFunction<Map<String, BigDecimal>, CurrencyValue, Map<String, BigDecimal>> { rates, currentCurrency ->
                val rebasedRates = convertManager.rebase(currentCurrency.currency, rates)
                convertManager.calculate(currentCurrency.value, rebasedRates)
            })
            .startWith(storage.getCurrencyValues())
            .doOnNext { storage.putCurrencyValues(it) }
    }
}
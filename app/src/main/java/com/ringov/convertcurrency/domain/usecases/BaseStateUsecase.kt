package com.ringov.convertcurrency.domain.usecases

import androidx.annotation.CallSuper
import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import io.reactivex.Scheduler
import io.reactivex.processors.BehaviorProcessor

/**
 * Must be used for usecases which provide a state of application
 */
abstract class BaseStateUsecase<T> {

    private val state = BehaviorProcessor.create<T>()

    fun update(changes: T) {
        state.onNext(changes)
    }

    fun observe(scheduler: Scheduler): Flowable<T> =
        state.onBackpressureLatest().observeOn(scheduler).compose(modification(scheduler))

    protected fun getValue(): T? = state.value

    private fun modification(scheduler: Scheduler): FlowableTransformer<T, T> =
        FlowableTransformer { this.modify(it, scheduler) }

    /**
     * Override this method for getting control over data flow
     */
    @CallSuper
    protected open fun modify(upstream: Flowable<T>, scheduler: Scheduler): Flowable<T> = upstream
}
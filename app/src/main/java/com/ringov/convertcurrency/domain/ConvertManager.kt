package com.ringov.convertcurrency.domain

import com.ringov.convertcurrency.utils.Logger
import java.math.BigDecimal
import java.math.RoundingMode

class ConvertManager {

    companion object {
        private const val PRECISION = 10
    }

    fun rebase(newBase: String, rates: Map<String, BigDecimal>): Map<String, BigDecimal> {
        val value = rates[newBase] ?: throw IllegalStateException("Could not find base rate in the map: $rates")
        if (value.isZero()) {
            Logger.w("Rate must not be 0, check the source of it")
            return rates
        }
        return rates.mapValues { it.value.divide(value, PRECISION, RoundingMode.HALF_UP) }
    }

    fun calculate(value: BigDecimal, rates: Map<String, BigDecimal>): Map<String, BigDecimal> =
        rates.mapValues { convert(value, it.value) }

    private fun convert(value: BigDecimal, rate: BigDecimal): BigDecimal {
        if (value.isZero()) {
            return BigDecimal.ZERO
        }
        return value.multiply(rate).precise()
    }

    private fun BigDecimal.isZero(): Boolean = this.compareTo(BigDecimal.ZERO) == 0

    private fun BigDecimal.precise(): BigDecimal = this.setScale(PRECISION, RoundingMode.HALF_UP)
}
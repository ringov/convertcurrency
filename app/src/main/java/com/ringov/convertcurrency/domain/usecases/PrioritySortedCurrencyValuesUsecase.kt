package com.ringov.convertcurrency.domain.usecases

import com.ringov.convertcurrency.data.CurrenciesRepo
import com.ringov.convertcurrency.domain.CurrencyValue
import com.ringov.convertcurrency.utils.RxSchedulers
import io.reactivex.Flowable
import io.reactivex.functions.Function3
import java.math.BigDecimal
import java.util.*
import kotlin.collections.HashMap

class PrioritySortedCurrencyValuesUsecase(
    private val currencyValuesUsecase: CurrencyValuesUsecase,
    private val currencyPriorityUsecase: CurrencyPriorityUsecase,
    private val currenciesRepo: CurrenciesRepo,
    private val rxSchedulers: RxSchedulers
) {
    fun observe(): Flowable<List<CurrencyValue>> {
        return Flowable.combineLatest(currencyValuesUsecase.observe(),
            currenciesRepo.getCurrencies().toFlowable().startWith(HashMap()),
            currencyPriorityUsecase.observe(rxSchedulers.io),
            Function3 { values, currencies, currency -> sort(values, currencies, currency) })
    }

    private fun sort(
        rates: Map<String, BigDecimal>,
        currencies: Map<String, String>,
        currencyPriority: List<String>
    ): List<CurrencyValue> =
        rates.map { CurrencyValue(it.key, it.value, currencies[it.key] ?: "") }
            .sortedWith(Comparator { v1, v2 ->
                val p1 = currencyPriority.indexOf(v1.currency)
                val p2 = currencyPriority.indexOf(v2.currency)
                val result = -(p1.compareTo(p2))
                if (result == 0) {
                    return@Comparator v1.currency.compareTo(v2.currency)
                }
                return@Comparator result
            })
}
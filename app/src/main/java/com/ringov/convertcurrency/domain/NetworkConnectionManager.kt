package com.ringov.convertcurrency.domain

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import com.ringov.convertcurrency.domain.usecases.BaseStateUsecase
import io.reactivex.Scheduler
import io.reactivex.Single

class NetworkConnectionManager : BaseStateUsecase<Boolean>() {

    companion object {
        private fun isConnected(context: Context, intent: Intent? = null): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = connectivityManager.activeNetworkInfo
            val noConnectivity =
                intent?.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false) ?: false
            return !noConnectivity && activeNetwork != null && activeNetwork.isConnected
        }
    }

    private val broadcastListener = NetworkBroadcastListener(this::update)

    fun register(context: Context) {
        context.registerReceiver(broadcastListener, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        update(isConnected(context))
    }

    fun unregister(context: Context) {
        context.unregisterReceiver(broadcastListener)
    }

    fun waitForNetwork(scheduler: Scheduler): Single<Unit> =
        observe(scheduler).filter { it }.take(1).singleOrError().map { Unit }

    private class NetworkBroadcastListener(private val callback: (Boolean) -> Unit) : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            callback(NetworkConnectionManager.isConnected(context, intent))
        }
    }
}
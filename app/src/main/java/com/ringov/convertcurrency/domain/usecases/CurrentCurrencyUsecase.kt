package com.ringov.convertcurrency.domain.usecases

import com.ringov.convertcurrency.data.PersistentStorage
import com.ringov.convertcurrency.domain.CurrencyValue
import io.reactivex.Flowable
import io.reactivex.Scheduler
import java.math.BigDecimal

class CurrentCurrencyUsecase(private val storage: PersistentStorage) : BaseStateUsecase<CurrencyValue>() {

    fun update(currency: String, value: BigDecimal) {
        update(CurrencyValue(currency, value))
    }

    override fun modify(upstream: Flowable<CurrencyValue>, scheduler: Scheduler): Flowable<CurrencyValue> {
        return super.modify(upstream, scheduler).startWith(storage.loadCurrentCurrency())
            .doOnNext { storage.saveCurrentCurrency(it) }
    }
}
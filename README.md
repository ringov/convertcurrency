## Currency Converter

**[Google Play](https://play.google.com/store/apps/details?id=com.ringov.convertcurrency)**

A simple application for converting currencies.

Classic stack of libraries and approaches: Dagger, Retrofit, RxJava, Usecases, MVP.

Third-party libraries:
- [Retrofit](https://github.com/square/retrofit)
- [RxJava](https://github.com/ReactiveX/RxJava)
- [RxAndroid](https://github.com/ReactiveX/RxAndroid)
- [Dagger](https://github.com/google/dagger)

![Screenshots](https://gitlab.com/ringov/convertcurrency/raw/master/materials/screenshot.jpg)